package com.maciejuk.ble.processing

import android.content.Context
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import java.util.concurrent.TimeUnit

private val processRequest = PeriodicWorkRequestBuilder<ProcessingWorker>(1, TimeUnit.DAYS).build()



fun scheduleProcessing(context: Context) = WorkManager.getInstance(context).enqueue(processRequest)

fun cancelProcessing(context: Context) = WorkManager.getInstance(context).cancelWorkById(processRequest.id)
