package com.maciejuk.ble.processing

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.maciejuk.ble.repository.DeviceRepository
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import org.threeten.bp.LocalDateTime

class ProcessingWorker(
    appContext: Context, workerParams: WorkerParameters
) : CoroutineWorker(appContext, workerParams), KoinComponent {
    private val repository: DeviceRepository by inject()


    override suspend fun doWork(): Result {
        val unprocessed = repository.loadUnprocessed()

        val processed = unprocessed.map { it.copy(processedTime = LocalDateTime.now()) }

        val updated = repository.update(processed)

        return if (updated == processed.size) Result.success() else Result.failure()
    }
}
