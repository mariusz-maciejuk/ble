package com.maciejuk.ble.discover

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.maciejuk.ble.appnavigation.Router
import com.maciejuk.ble.ble.scanner.BleScanner
import com.maciejuk.ble.discover.model.DiscoverState
import com.maciejuk.ble.domain.model.Device
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

class DiscoverViewModel(
    dispatcher: CoroutineDispatcher,
    private val router: Router,
    private val bleScanner: BleScanner
) : ViewModel(), LifecycleObserver, CoroutineScope {
    companion object {
        const val BT_ENABLE_REQUEST = 11
    }

    private val job = Job()
    override val coroutineContext: CoroutineContext = dispatcher + job

    val state: LiveData<DiscoverState>
        get() = _state
    private val _state = MutableLiveData<DiscoverState>()

    private val discovering: Boolean
        get() = state.value?.discovering ?: false


    init {
        _state.value = DiscoverState(
            discovering = false,
            discoveredDevices = emptySet(),
            loading = false
        )

        bleScanner.listener = ::onDeviceDiscovered
    }


    override fun onCleared() {
        job.cancel()
        super.onCleared()
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onActivityResume() {
        if (discovering) bleScanner.startScan()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onActivityPause() {
        if (discovering) bleScanner.stopScan()
    }


    fun onBtEnabled() {
        onStartStopClicked()
    }

    fun onStartStopClicked() {
        if (!discovering && !bleScanner.btEnabled) { router.showBtSettings(BT_ENABLE_REQUEST); return }

        _state.update(
            discovering = !discovering,
            loading = !discovering
        )

        bleScanner.run { if (discovering) startScan() else stopScan() }
    }

    fun onDeviceDetailsRequested(device: Device) {
        router.showDetails(device.address)
    }

    fun quit() {
        router.closeCurrentScreen()
    }

    fun onDeviceDiscovered(device: Device) {
        _state.update(devices = state.value?.discoveredDevices.orEmpty() + device)
    }


    private fun handleException(exception: Exception) {
        Timber.tag(DiscoverViewModel::class.simpleName).e(exception)
        _state.update(exception = exception)
    }
}


private fun MutableLiveData<DiscoverState>.update(
    discovering: Boolean = value?.discovering ?: false,
    devices: Set<Device> = value?.discoveredDevices.orEmpty(),
    loading: Boolean = value?.loading ?: false,
    exception: Exception? = value?.exception
) {
    @Suppress("CopyWithoutNamedArguments")
    value = value?.copy(discovering, devices, loading, exception)
}
