package com.maciejuk.ble.discover

import android.Manifest
import android.content.Intent
import android.content.Intent.ACTION_MAIN
import android.content.Intent.CATEGORY_LAUNCHER
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.maciejuk.ble.R
import com.maciejuk.ble.appnavigation.NavigationDrawerHandler
import com.maciejuk.ble.common.extensions.allPermissionsGranted
import com.maciejuk.ble.common.extensions.bind
import com.maciejuk.ble.common.extensions.injectPassingActivity
import com.maciejuk.ble.common.extensions.toast
import com.maciejuk.ble.common.widgets.SearchableTappableList
import com.maciejuk.ble.databinding.ActivityDiscoverBinding
import com.maciejuk.ble.discover.model.DiscoverState
import com.maciejuk.ble.domain.includeInSearch
import com.maciejuk.ble.domain.model.Device
import com.snakydesign.livedataextensions.distinctUntilChanged
import com.snakydesign.livedataextensions.map
import kotlinx.android.synthetic.main.activity_discover.discoveredDevicesList
import kotlinx.android.synthetic.main.view_appbar.toolbar
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class DiscoverActivity : AppCompatActivity() {
    companion object {
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION)
    }

    private val navigationDrawerHandler: NavigationDrawerHandler by injectPassingActivity()
    private val viewModel: DiscoverViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!isTaskRoot && intent.hasCategory(CATEGORY_LAUNCHER) && intent.action == ACTION_MAIN) {
            finish(); return
        }

        bind<ActivityDiscoverBinding>(R.layout.activity_discover) {
            viewModel = this@DiscoverActivity.viewModel
            lifecycleOwner = this@DiscoverActivity
        }
        lifecycle.addObserver(viewModel)

        setSupportActionBar(toolbar)
        supportActionBar?.setTitle(R.string.discover_title)
        lifecycle.addObserver(navigationDrawerHandler)

        viewModel.state.observe(this, Observer { Timber.d("$it") })

        setupDevicesList()
    }

    override fun onBackPressed() {
        viewModel.quit()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (!allPermissionsGranted(permissions))
            toast(R.string.permission_denied)
        else
            viewModel.onStartStopClicked()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == DiscoverViewModel.BT_ENABLE_REQUEST && resultCode == RESULT_OK)
            viewModel.onBtEnabled()

        super.onActivityResult(requestCode, resultCode, data)
    }

    fun onStartStopClicked(@Suppress("UNUSED_PARAMETER") view: View) {
        if (allPermissionsGranted(REQUIRED_PERMISSIONS))
            viewModel.onStartStopClicked()
        else
            requestPermissions(REQUIRED_PERMISSIONS, 0)
    }


    private fun setupDevicesList() = discoveredDevicesList.apply {
        registerType(R.layout.view_item_device, object : SearchableTappableList.CallBack<Device> {
            override fun onValueSelected(value: Device) = viewModel.onDeviceDetailsRequested(value)
            override fun includeInSearch(item: Device, query: CharSequence) = item.includeInSearch(query)
        })

        viewModel.state
            .map { it.discoveredDevices }
            .distinctUntilChanged()
            .observe(this@DiscoverActivity, Observer<Set<Device>> {
                data = it.toList()
            })
    }
}
