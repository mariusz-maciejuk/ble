package com.maciejuk.ble.discover.model

import com.maciejuk.ble.domain.model.Device

data class DiscoverState(
    val discovering: Boolean,
    val discoveredDevices: Set<Device>,
    val loading: Boolean,
    val exception: Exception? = null
)
