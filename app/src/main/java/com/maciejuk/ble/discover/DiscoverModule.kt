package com.maciejuk.ble.discover

import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val discoverModule = module {
    viewModel { DiscoverViewModel(get("main"), get(), get()) }
}
