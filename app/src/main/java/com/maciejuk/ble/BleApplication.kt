package com.maciejuk.ble

import android.app.Application
import android.content.Context
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.jakewharton.threetenabp.AndroidThreeTen
import com.maciejuk.ble.appnavigation.Router
import com.maciejuk.ble.appnavigation.appNavigationModule
import com.maciejuk.ble.auth.authModule
import com.maciejuk.ble.ble.bleModule
import com.maciejuk.ble.dbpeek.dbPeekModule
import com.maciejuk.ble.details.detailsModule
import com.maciejuk.ble.discover.discoverModule
import com.maciejuk.ble.identity.IdentityProvider
import com.maciejuk.ble.processing.scheduleProcessing
import com.maciejuk.ble.repository.repositoryModule
import com.maciejuk.ble.settings.settingsModule
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import org.koin.android.ext.android.get
import org.koin.android.ext.android.startKoin
import org.koin.dsl.module.module
import timber.log.Timber

@Suppress("unused")
class BleApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        AndroidThreeTen.init(this)
        startKoin(this, activeModules)
        get<Router>().init(this)
        get<IdentityProvider>().init(this)

        scheduleProcessing(this)
    }
}


private val appModule = module {
    single<CoroutineDispatcher>("main") { Dispatchers.Main }
    single("io") { Dispatchers.IO }

    single("main") { get<Context>().mainLooper }

    single { LocalBroadcastManager.getInstance(get()) }

    single { IdentityProvider() }
}

private val activeModules = listOf(
    /* utils */ appModule, appNavigationModule, bleModule, repositoryModule,
    /* ui */ authModule, dbPeekModule, detailsModule, discoverModule, settingsModule
)