package com.maciejuk.ble.common.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.LinearLayout
import androidx.annotation.LayoutRes
import androidx.appcompat.widget.SearchView
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.INVALID_TYPE
import com.maciejuk.ble.BR
import com.maciejuk.ble.R
import com.maciejuk.ble.common.extensions.setVisibility
import kotlinx.android.synthetic.main.widget_searchable_tappable_list.view.stlEmptyLabel
import kotlinx.android.synthetic.main.widget_searchable_tappable_list.view.stlList
import kotlinx.android.synthetic.main.widget_searchable_tappable_list.view.stlSearch
import kotlin.properties.Delegates
import kotlin.reflect.KClass

class SearchableTappableList(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs),
    SearchView.OnQueryTextListener {

    var data by Delegates.observable<List<Any>?>(null) { _, _, newValue ->
        stlSearch.setQuery(null, false)
        adapter.displayedData = newValue
        render()
    }
    var searchEnabled by Delegates.observable(false) { _, _, newValue -> stlSearch.setVisibility(newValue) }
    var emptyLabel by Delegates.observable("") { _, _, _ -> render() }

    val registeredTypes: MutableMap<KClass<*>, Pair<Int, CallBack<*>>> = mutableMapOf()

    private val adapter = Adapter()
    private val filter = SessionDataFilter()


    init {
        setRootProperties()
        LayoutInflater.from(context).inflate(R.layout.widget_searchable_tappable_list, this, true)

        obtainAttributes(context, attrs)

        stlList.apply {
            setHasFixedSize(true)
            adapter = this@SearchableTappableList.adapter
        }

        stlSearch.setOnQueryTextListener(this)
    }


    override fun onQueryTextSubmit(query: String?) = false

    override fun onQueryTextChange(query: String?) = if (query != null && query.isNotEmpty() && data != null) {
        filter.filter(query)
        true
    } else {
        adapter.displayedData = data
        false
    }


    inline fun <reified T : Any> registerType(
        @LayoutRes rowLayoutId: Int,
        callback: CallBack<T> = object : CallBack<T> {}
    ) {
        registeredTypes[T::class] = Pair(rowLayoutId, callback)
    }


    private fun setRootProperties() {
        orientation = VERTICAL
    }

    private fun obtainAttributes(context: Context, attrs: AttributeSet) = context.theme.obtainStyledAttributes(
        attrs, R.styleable.SearchableTappableList, 0, R.style.SearchableTappableListStyle
    ).run {
        try {
            searchEnabled = getBoolean(R.styleable.SearchableTappableList_stl_searchEnabled, false)
            emptyLabel = getString(R.styleable.SearchableTappableList_stl_emptyLabel).orEmpty()
        } finally {
            recycle()
        }
    }

    private fun render() {
        stlEmptyLabel.apply {
            text = emptyLabel
            setVisibility(data.isNullOrEmpty() && emptyLabel.isNotEmpty())
        }
    }


    interface CallBack<in T : Any> {
        fun onValueSelected(value: T) { }
        fun includeInSearch(item: T, query: CharSequence): Boolean = false
    }


    private inner class Adapter : RecyclerView.Adapter<Adapter.SessionDataOptionViewHolder>() {
        var displayedData by Delegates.observable<List<Any>?>(null) { _, _, _ -> notifyDataSetChanged() }


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SessionDataOptionViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = DataBindingUtil.inflate<ViewDataBinding>(
                layoutInflater, viewType, parent, false
            )
            return SessionDataOptionViewHolder(binding)
        }

        override fun onBindViewHolder(holder: SessionDataOptionViewHolder, position: Int) {
            holder.bind(getItemForPosition(position))
        }

        override fun getItemViewType(position: Int) = registeredTypes[getItemForPosition(position)::class]?.first
            ?: INVALID_TYPE

        override fun getItemCount() = displayedData?.size ?: 0


        private fun getItemForPosition(position: Int) = displayedData!![position]


        private inner class SessionDataOptionViewHolder(private val binding: ViewDataBinding) :
            RecyclerView.ViewHolder(binding.root) {
            fun bind(item: Any) {
                binding.run {
                    setVariable(BR.item, item)
                    setVariable(BR.callback, registeredTypes[item::class]?.second)
                    executePendingBindings()
                }
            }
        }
    }


    @Suppress("UNCHECKED_CAST")
    private inner class SessionDataFilter : Filter() {
        override fun performFiltering(constraint: CharSequence) = FilterResults().also {
            it.values = data?.filter {
                (registeredTypes[it::class]?.second as CallBack<Any>).includeInSearch(it, constraint)
            }.orEmpty()
            it.count = (it.values as List<Any>).size
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            adapter.displayedData = results.values as List<Any>
        }
    }
}


@BindingAdapter("stl_emptyLabel")
fun SearchableTappableList.setStlEmptyLabel(value: String?) {
    emptyLabel = value.orEmpty()
}
