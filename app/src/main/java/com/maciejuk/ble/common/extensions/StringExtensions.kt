package com.maciejuk.ble.common.extensions

import java.util.UUID

fun String.toUuid() = UUID.fromString(this)
