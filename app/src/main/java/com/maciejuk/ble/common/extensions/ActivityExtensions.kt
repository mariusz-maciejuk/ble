package com.maciejuk.ble.common.extensions

import android.app.Activity
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

fun <T : ViewDataBinding> Activity.bind(@LayoutRes res: Int, lambda: (T.() -> Unit)? = null): T =
    DataBindingUtil.setContentView<T>(this, res).apply { lambda?.invoke(this) }
