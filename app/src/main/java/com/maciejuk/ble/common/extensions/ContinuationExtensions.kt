package com.maciejuk.ble.common.extensions

import kotlin.coroutines.Continuation
import kotlin.coroutines.resume

inline fun <reified T> Continuation<T>.resumeWithNull() = resume(null as T)
