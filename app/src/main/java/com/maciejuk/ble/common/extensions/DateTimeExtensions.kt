package com.maciejuk.ble.common.extensions

import org.threeten.bp.LocalDateTime
import org.threeten.bp.LocalTime
import org.threeten.bp.ZoneId
import org.threeten.bp.ZoneOffset
import org.threeten.bp.ZonedDateTime

fun LocalDateTime.toUtcZone(currentZone: ZoneId = ZoneId.systemDefault()) =
    ZonedDateTime.of(this, currentZone).withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime()!!

fun LocalDateTime.toCurrentZone(currentZone: ZoneId = ZoneId.systemDefault()) =
    ZonedDateTime.of(this, ZoneOffset.UTC).withZoneSameInstant(currentZone).toLocalDateTime()!!
