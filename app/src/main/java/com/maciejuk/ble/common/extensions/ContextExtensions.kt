package com.maciejuk.ble.common.extensions

import android.content.Context
import android.content.pm.PackageManager
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat

fun Context.toast(@StringRes stringResId: Int, length: Int = Toast.LENGTH_LONG) =
    Toast.makeText(this, stringResId, length).show()

fun Context.toast(string: String, length: Int = Toast.LENGTH_LONG) = Toast.makeText(this, string, length).show()

fun Context.allPermissionsGranted(permissions: Array<String>) = permissions.all {
    ContextCompat.checkSelfPermission(this, it) == PackageManager.PERMISSION_GRANTED
}
