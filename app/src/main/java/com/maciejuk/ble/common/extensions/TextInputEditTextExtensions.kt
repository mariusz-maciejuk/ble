package com.maciejuk.ble.common.extensions

import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputEditText

@BindingAdapter("initial_value")
fun TextInputEditText.setInitialValue(initialValue: String?) {
    if (!initialValue.isNullOrBlank() && text?.isBlank() == true) setText(initialValue)
}
