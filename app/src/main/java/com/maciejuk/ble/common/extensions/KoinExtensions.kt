package com.maciejuk.ble.common.extensions

import android.app.Activity
import org.koin.android.ext.android.inject
import org.koin.core.parameter.ParameterList
import org.koin.core.parameter.parametersOf

fun <K, V> composeParams(vararg pairs: Pair<K, V>) = parametersOf(mapOf(*pairs))

operator fun <T> ParameterList.get(name: String): T = (get(0) as Map<String, T>).getValue(name)

inline fun <reified T : Any> Activity.injectPassingActivity() = inject<T> { parametersOf(this) }
