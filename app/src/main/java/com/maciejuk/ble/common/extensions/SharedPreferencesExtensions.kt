package com.maciejuk.ble.common.extensions

import android.content.SharedPreferences

fun SharedPreferences.save(key: String, value: String) = edit()
    .putString(key, value)
    .apply()
