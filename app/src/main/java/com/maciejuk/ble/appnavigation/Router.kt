package com.maciejuk.ble.appnavigation

import android.app.Activity
import android.app.Application
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TASK
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP
import android.os.Bundle
import com.maciejuk.ble.auth.AuthActivity
import com.maciejuk.ble.dbpeek.DbPeekActivity
import com.maciejuk.ble.details.DetailsActivity
import com.maciejuk.ble.details.withDetailsParams
import com.maciejuk.ble.discover.DiscoverActivity
import com.maciejuk.ble.settings.SettingsActivity
import java.lang.ref.WeakReference

class Router {
    var currentActivity: Activity?
        get() = _currentActivity?.get()
        private set(value) { _currentActivity = value?.let { WeakReference(value) } }
    private var _currentActivity: WeakReference<Activity>? = null

    private val activityWatcher = ActivityWatcher()


    fun init(application: Application) {
        application.registerActivityLifecycleCallbacks(activityWatcher)
    }


    fun closeCurrentScreen() = currentActivity?.finish()

    fun showDiscover() = start<DiscoverActivity> {
        addFlags(FLAG_ACTIVITY_BROUGHT_TO_FRONT or FLAG_ACTIVITY_CLEAR_TASK or FLAG_ACTIVITY_CLEAR_TOP)
    }

    fun showDetails(address: String) = start<DetailsActivity> { withDetailsParams(address) }

    fun showSettings() = start<SettingsActivity> {}

    fun showAuth() = start<AuthActivity> {}

    fun showDbPeek() = start<DbPeekActivity> {}

    fun showBtSettings(requestCode: Int) {
        currentActivity?.startActivityForResult(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), requestCode)
    }


    private inline fun <reified TargetActivity : Activity> start(crossinline params: Intent.() -> Unit) =
        currentActivity?.run { runOnUiThread { startActivity(createIntent<TargetActivity>().apply { params() }) } }

    private inline fun <reified TargetActivity : Activity> createIntent(): Intent =
        Intent(currentActivity, TargetActivity::class.java)


    private inner class ActivityWatcher : Application.ActivityLifecycleCallbacks {
        override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
            currentActivity = activity
        }
        override fun onActivityResumed(activity: Activity) {
            currentActivity = activity
        }
        override fun onActivityStarted(activity: Activity) {}
        override fun onActivityPaused(activity: Activity) {}
        override fun onActivityStopped(activity: Activity) {}
        override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle?) {}
        override fun onActivityDestroyed(activity: Activity) {}
    }
}
