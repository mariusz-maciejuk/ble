package com.maciejuk.ble.appnavigation

import android.app.Activity
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_LOCKED_CLOSED
import androidx.drawerlayout.widget.DrawerLayout.LOCK_MODE_UNLOCKED
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.google.android.material.navigation.NavigationView
import com.maciejuk.ble.R

class NavigationDrawerHandler(
    activity: Activity,
    private val router: Router
) : NavigationView.OnNavigationItemSelectedListener, LifecycleObserver {
    var enabled: Boolean
        get() = toggle.isDrawerIndicatorEnabled
        set(value) {
            toggle.isDrawerIndicatorEnabled = value
            drawerLayout.setDrawerLockMode(if (value) LOCK_MODE_UNLOCKED else LOCK_MODE_LOCKED_CLOSED)
        }

    private val toggle: ActionBarDrawerToggle by lazy {
        ActionBarDrawerToggle(
            activity, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
    }
    private val toolbar by lazy {
        activity.findViewById<Toolbar>(R.id.toolbar)
            ?: throw IllegalStateException("$activity doesn't have Toolbar (ID: toolbar)")
    }
    private val drawerLayout by lazy {
        activity.findViewById<DrawerLayout>(R.id.drawerLayout)
            ?: throw IllegalStateException("$activity doesn't have DrawerLayout (ID: drawerLayout)")
    }
    private val navigationView by lazy {
        activity.findViewById<NavigationView>(R.id.navigationView)
            ?: throw IllegalStateException("$activity doesn't have NavigationView (ID: navigationView)")
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        router.run { when (item.itemId) {
            R.id.nav_discover -> showDiscover()
            R.id.nav_auth -> showAuth()
            R.id.nav_settings -> showSettings()
            R.id.nav_dbpeek -> showDbPeek()
            else -> throw IllegalStateException("${item.itemId} MenuItem not supported")
        } }

        drawerLayout.closeDrawers()

        return false
    }


    @Suppress("unused")
    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onActivityCreate() {
        setUpToolbarToggle()
        navigationView.setNavigationItemSelectedListener(this)
    }


    private fun setUpToolbarToggle() = toggle.let {
        drawerLayout.addDrawerListener(it)
        it.syncState()
    }
}
