package com.maciejuk.ble.appnavigation

import android.app.Activity
import org.koin.dsl.module.module

val appNavigationModule = module {
    single { Router() }
    factory { (activity: Activity) -> NavigationDrawerHandler(activity, get()) }
}
