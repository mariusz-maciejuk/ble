package com.maciejuk.ble.auth

import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val authModule = module {
    viewModel { AuthViewModel(get("main"), get(), get()) }
}
