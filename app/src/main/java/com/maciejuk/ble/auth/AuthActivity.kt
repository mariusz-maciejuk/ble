package com.maciejuk.ble.auth

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.maciejuk.ble.R
import com.maciejuk.ble.appnavigation.NavigationDrawerHandler
import com.maciejuk.ble.auth.AuthViewModel.Companion.SIGN_IN_REQUEST
import com.maciejuk.ble.common.extensions.bind
import com.maciejuk.ble.common.extensions.injectPassingActivity
import com.maciejuk.ble.databinding.ActivityAuthBinding
import com.maciejuk.ble.identity.getSignInResult
import com.snakydesign.livedataextensions.distinctUntilChanged
import com.snakydesign.livedataextensions.map
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_auth.signInButton
import kotlinx.android.synthetic.main.activity_auth.userAvatar
import kotlinx.android.synthetic.main.view_appbar.toolbar
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class AuthActivity : AppCompatActivity() {
    private val navigationDrawerHandler: NavigationDrawerHandler by injectPassingActivity()
    private val viewModel: AuthViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bind<ActivityAuthBinding>(R.layout.activity_auth) {
            viewModel = this@AuthActivity.viewModel
            lifecycleOwner = this@AuthActivity
        }
        lifecycle.addObserver(viewModel)

        setSupportActionBar(toolbar)
        lifecycle.addObserver(navigationDrawerHandler)

        viewModel.state.observe(this, Observer { Timber.d("$it") })

        signInButton.setOnClickListener { viewModel.signInRequested() }

        viewModel.state
            .map { it.user?.pictureUrl }
            .distinctUntilChanged()
            .observe(this, Observer {
                Picasso.get().load(it).into(userAvatar)
            })
    }

    override fun onBackPressed() {
        viewModel.quit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SIGN_IN_REQUEST) viewModel.onSignInCompleted(getSignInResult(data))

        super.onActivityResult(requestCode, resultCode, data)
    }
}
