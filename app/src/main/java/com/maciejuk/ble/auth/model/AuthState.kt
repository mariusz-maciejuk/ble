package com.maciejuk.ble.auth.model

import com.maciejuk.ble.domain.model.User

data class AuthState(
    val user: User?,
    val loading: Boolean,
    val exception: Exception? = null
)
