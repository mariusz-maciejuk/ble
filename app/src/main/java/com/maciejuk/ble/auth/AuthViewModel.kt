package com.maciejuk.ble.auth

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.maciejuk.ble.appnavigation.Router
import com.maciejuk.ble.auth.model.AuthState
import com.maciejuk.ble.domain.model.User
import com.maciejuk.ble.identity.IdentityProvider
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

class AuthViewModel(
    dispatcher: CoroutineDispatcher,
    private val router: Router,
    private val identityProvider: IdentityProvider
) : ViewModel(), LifecycleObserver, CoroutineScope {
    companion object {
        const val SIGN_IN_REQUEST = 21
    }

    private val job = Job()
    override val coroutineContext: CoroutineContext = dispatcher + job

    val state: LiveData<AuthState>
        get() = _state
    private val _state = MutableLiveData<AuthState>()


    init {
        _state.value = AuthState(
            user = null,
            loading = false
        )
    }


    override fun onCleared() {
        job.cancel()
        super.onCleared()
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onActivityStart() {
        _state.update(user = identityProvider.getLastSigned())
    }


    fun signInRequested() {
        _state.update(loading = true)
        identityProvider.requestSignIn(SIGN_IN_REQUEST)
    }

    fun onSignInCompleted(user: User?) {
        _state.update(
            user = user,
            loading = false
        )
    }

    fun signOutRequested() {
        signOut()
    }

    fun quit() {
        router.closeCurrentScreen()
    }


    private fun signOut() = launch {
        _state.update(loading = true)
        try {
            identityProvider.run {
                if (requestSignOut()) requestRevokeAccess()
            }
            _state.update(user = null)
        } catch (exception: Exception) {
            handleException(exception)
        } finally {
            _state.update(loading = false)
        }
    }

    private fun handleException(exception: Exception) {
        Timber.tag(AuthViewModel::class.simpleName).e(exception)
        _state.update(exception = exception)
    }
}


private fun MutableLiveData<AuthState>.update(
    user: User? = value?.user,
    loading: Boolean = value?.loading ?: false,
    exception: Exception? = value?.exception
) {
    @Suppress("CopyWithoutNamedArguments")
    value = value?.copy(user, loading, exception)
}
