package com.maciejuk.ble.domain

import com.maciejuk.ble.domain.model.Device

fun Device.includeInSearch(query: CharSequence) = name.contains(query, true) || address.contains(query, true)
