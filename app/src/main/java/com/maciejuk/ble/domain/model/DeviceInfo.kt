package com.maciejuk.ble.domain.model

import org.threeten.bp.LocalDateTime

data class DeviceInfo(
    val id: Long?,
    val address: String,
    val manufacturerName: String?,
    val deviceName: String?,
    val serialNumber: String?,
    val batteryLevel: Int?,
    val saveTime: LocalDateTime?,
    val processedTime: LocalDateTime?
)
