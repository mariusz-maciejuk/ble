package com.maciejuk.ble.domain.model

data class Device(
    val name: String,
    val address: String
)
