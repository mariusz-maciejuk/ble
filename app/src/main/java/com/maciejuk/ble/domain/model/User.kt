package com.maciejuk.ble.domain.model

data class User(
    val name: String,
    val pictureUrl: String
)
