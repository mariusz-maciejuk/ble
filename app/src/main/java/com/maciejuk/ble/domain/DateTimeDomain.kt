package com.maciejuk.ble.domain

import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

private val dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy.MM.dd hh:mm:ss")

fun LocalDateTime?.toFormattedString(): String =
    if (this != null) format(dateTimeFormatter) else ""
