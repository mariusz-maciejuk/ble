package com.maciejuk.ble.identity

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.os.Bundle
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInOptions.DEFAULT_SIGN_IN
import com.google.android.gms.common.api.ApiException
import com.maciejuk.ble.domain.model.User
import timber.log.Timber
import java.lang.ref.WeakReference
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class IdentityProvider {
    private var currentActivity: Activity?
        get() = _currentActivity?.get()
        set(value) { _currentActivity = value?.let { WeakReference(value) } }
    private var _currentActivity: WeakReference<Activity>? = null

    private val activityWatcher = ActivityWatcher()

    private val signInOptions = GoogleSignInOptions.Builder(DEFAULT_SIGN_IN).build()
    private val googleClient: GoogleSignInClient?
        get() = currentActivity?.let { GoogleSignIn.getClient(it, signInOptions) }


    fun init(application: Application) {
        application.registerActivityLifecycleCallbacks(activityWatcher)
    }

    fun requestSignIn(requestCode: Int) {
        currentActivity?.startActivityForResult(googleClient?.signInIntent, requestCode)
    }

    suspend fun requestSignOut(): Boolean = suspendCoroutine { cont ->
        currentActivity?.let {
            googleClient?.signOut()?.addOnCompleteListener(it) { task ->
                cont.resume(task.isSuccessful)
            }
        }
    }

    suspend fun requestRevokeAccess(): Boolean = suspendCoroutine { cont ->
        currentActivity?.let {
            googleClient?.revokeAccess()?.addOnCompleteListener(it) { task ->
                cont.resume(task.isSuccessful)
            }
        }
    }

    fun getLastSigned(): User? {
        return currentActivity?.let { GoogleSignIn.getLastSignedInAccount(it) }?.toUser()
    }


    private inner class ActivityWatcher : Application.ActivityLifecycleCallbacks {
        override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
            currentActivity = activity
        }
        override fun onActivityResumed(activity: Activity) {
            currentActivity = activity
        }
        override fun onActivityStarted(activity: Activity) {}
        override fun onActivityPaused(activity: Activity) {}
        override fun onActivityStopped(activity: Activity) {}
        override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle?) {}
        override fun onActivityDestroyed(activity: Activity) {}
    }
}


fun getSignInResult(intent: Intent?): User? {
    val task = GoogleSignIn.getSignedInAccountFromIntent(intent)
    return try {
        task.getResult(ApiException::class.java)
    } catch (e: Exception) {
        Timber.e(e)
        null
    }?.toUser()
}


private fun GoogleSignInAccount.toUser() = User(
    name = displayName.orEmpty(),
    pictureUrl = photoUrl.toString()
)
