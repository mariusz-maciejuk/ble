package com.maciejuk.ble.ble.connection

import android.bluetooth.BluetoothGattCharacteristic.FORMAT_UINT8
import com.maciejuk.ble.ble.GattIds.CHARACTERISTIC_ID_BATTERY_LEVEL
import com.maciejuk.ble.ble.GattIds.CHARACTERISTIC_ID_DEVICE_NAME
import com.maciejuk.ble.ble.GattIds.CHARACTERISTIC_ID_MANUFACTURER_NAME
import com.maciejuk.ble.ble.GattIds.CHARACTERISTIC_ID_SERIAL_NUMBER
import com.maciejuk.ble.ble.GattIds.SERVICE_ID_BATTERY_INFO
import com.maciejuk.ble.ble.GattIds.SERVICE_ID_DEVICE_INFO
import com.maciejuk.ble.ble.GattIds.SERVICE_ID_GENERIC_ACCESS

class BleDeviceInfoReader(
    private val adapter: BleConnectionAdapter
) {
    var listener: BleConnectionAdapter.Listener?
        get() = adapter.listener
        set(value) { adapter.listener = value }


    fun connectToDevice(address: String) = adapter.connectToDevice(address)

    fun disconnect() = adapter.disconnect()


    suspend fun getManufacturerName(): String? = adapter.getCharacteristic(
        SERVICE_ID_DEVICE_INFO, CHARACTERISTIC_ID_MANUFACTURER_NAME
    )

    suspend fun getDeviceName(): String? = adapter.getCharacteristic(
        SERVICE_ID_GENERIC_ACCESS, CHARACTERISTIC_ID_DEVICE_NAME
    )

    suspend fun getSerialNumber(): String? = adapter.getCharacteristic(
        SERVICE_ID_DEVICE_INFO, CHARACTERISTIC_ID_SERIAL_NUMBER
    )

    suspend fun getBatteryLevel(): Int? = adapter.getCharacteristic(
        SERVICE_ID_BATTERY_INFO, CHARACTERISTIC_ID_BATTERY_LEVEL, FORMAT_UINT8
    )
}
