package com.maciejuk.ble.ble

import android.bluetooth.BluetoothManager
import android.content.Context
import androidx.appcompat.app.AppCompatActivity.BLUETOOTH_SERVICE
import com.maciejuk.ble.ble.connection.BleConnectionAdapter
import com.maciejuk.ble.ble.connection.BleDeviceInfoReader
import com.maciejuk.ble.ble.scanner.BleScanner
import org.koin.dsl.module.module

val bleModule = module {
    factory { (get<Context>().getSystemService(BLUETOOTH_SERVICE) as BluetoothManager).adapter }
    factory { BleScanner(get()) }
    factory { BleConnectionAdapter(get("main"), get(), get(), get()) }
    factory { BleDeviceInfoReader(get()) }
}
