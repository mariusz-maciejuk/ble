package com.maciejuk.ble.ble.connection.model

enum class ConnectionState {
    CONNECTED, DISCONNECTED
}