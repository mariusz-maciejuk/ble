package com.maciejuk.ble.ble.scanner

import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import timber.log.Timber

open class LoggingScanCallback : ScanCallback() {
    override fun onScanFailed(errorCode: Int) {
        Timber.v("onScanFailed: errorCode: $errorCode")
    }

    override fun onScanResult(callbackType: Int, result: ScanResult?) {
        Timber.v("onScanResult: callbackType: $callbackType, result: $result")
    }

    override fun onBatchScanResults(results: MutableList<ScanResult>?) {
        Timber.v("onBatchScanResults: results: $results")
    }
}
