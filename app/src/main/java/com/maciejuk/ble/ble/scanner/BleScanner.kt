package com.maciejuk.ble.ble.scanner

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanResult
import com.maciejuk.ble.domain.model.Device

class BleScanner(
    private val adapter: BluetoothAdapter
) {
    var listener: ((Device) -> Unit)? = null

    val btEnabled: Boolean
        get() = adapter.isEnabled

    private lateinit var scanner: BluetoothLeScanner
    private val scanCallback = BleScanCallback()


    fun startScan() {
        scanner = adapter.bluetoothLeScanner
        scanner.startScan(scanCallback)
    }

    fun stopScan() {
        scanner.stopScan(scanCallback)
    }



    private inner class BleScanCallback : LoggingScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)

            result?.device?.let { listener?.invoke(it.toDevice()) }
        }
    }
}


fun BluetoothDevice.toDevice() = Device(name.orEmpty(), address.orEmpty())
