package com.maciejuk.ble.ble.connection

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothDevice.TRANSPORT_LE
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothProfile.STATE_CONNECTED
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Handler
import android.os.Looper
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.maciejuk.ble.BuildConfig
import com.maciejuk.ble.ble.connection.model.ConnectionState
import com.maciejuk.ble.ble.connection.model.ConnectionState.CONNECTED
import com.maciejuk.ble.ble.connection.model.ConnectionState.DISCONNECTED
import com.maciejuk.ble.common.extensions.resumeWithNull
import com.maciejuk.ble.common.extensions.toUuid
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class BleConnectionAdapter(
    looper: Looper,
    val localBroadcastManager: LocalBroadcastManager,
    private val context: Context,
    private val adapter: BluetoothAdapter
) {
    companion object {
        const val BROADCAST_PARAM_CHARACTERISTIC =
            BuildConfig.APPLICATION_ID + ".bleconnectionadapter.broadcast.param.characteristic"
    }

    var listener: Listener? = null

    var device: BluetoothDevice? = null
        private set
    var gatt: BluetoothGatt? = null
        private set

    private val handler = Handler(looper)

    private val gattCallback = BleGattCallback()


    fun connectToDevice(address: String) {
        device = adapter.getRemoteDevice(address)
        gatt = device?.connectGatt(context, false, gattCallback, TRANSPORT_LE)
    }

    fun disconnect() {
        gatt?.disconnect()
        gatt = null
        device = null
    }

    suspend inline fun <reified T> getCharacteristic(
        serviceId: String, characteristicId: String, format: Int = 0, offset: Int = 0
    ): T = suspendCoroutine { cont ->
        gatt?.getService(serviceId.toUuid())?.let { service ->
            service.getCharacteristic(characteristicId.toUuid())?.let { characteristic ->
                gatt?.readCharacteristic(characteristic)
                localBroadcastManager.registerReceiver(object : BroadcastReceiver() {
                    override fun onReceive(context: Context, intent: Intent) {
                        localBroadcastManager.unregisterReceiver(this)
                        intent.getParcelableExtra<BluetoothGattCharacteristic>(BROADCAST_PARAM_CHARACTERISTIC)?.run {
                            @Suppress("IMPLICIT_CAST_TO_ANY")
                            val value = when(T::class) {
                                String::class -> getStringValue(offset)
                                Int::class -> getIntValue(format, offset)
                                Float::class -> getFloatValue(format, offset)
                                else -> cont.resumeWithException(
                                    UnsupportedOperationException("${T::class.simpleName} is not supported")
                                )
                            } as T
                            cont.resume(value)
                        }
                    }
                }, IntentFilter(characteristicId))
            } ?: cont.resumeWithNull()
        } ?: cont.resumeWithNull()
    }


    interface Listener {
        fun onConnectionStateChanged(connectionState: ConnectionState)
    }


    private inner class BleGattCallback : LoggingBluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)

            when (newState) {
                STATE_CONNECTED -> gatt.discoverServices()
                else -> handler.post { listener?.onConnectionStateChanged(DISCONNECTED) }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            super.onServicesDiscovered(gatt, status)

            handler.post { listener?.onConnectionStateChanged(CONNECTED) }
        }

        override fun onCharacteristicRead(
            gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic, status: Int
        ) {
            super.onCharacteristicRead(gatt, characteristic, status)

            localBroadcastManager.sendBroadcast(Intent(characteristic.uuid.toString()).also {
                it.putExtra(BROADCAST_PARAM_CHARACTERISTIC, characteristic)
            })
        }
    }
}
