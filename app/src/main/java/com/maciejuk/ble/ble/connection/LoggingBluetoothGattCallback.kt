package com.maciejuk.ble.ble.connection

import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCallback
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import timber.log.Timber

open class LoggingBluetoothGattCallback : BluetoothGattCallback() {
    override fun onReadRemoteRssi(gatt: BluetoothGatt, rssi: Int, status: Int) {
        Timber.v("onReadRemoteRssi: rssi: $rssi, status: $status")
    }

    override fun onCharacteristicRead(
        gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic, status: Int
    ) {
        Timber.v("onCharacteristicRead: characteristic: $characteristic, status: $status")
    }

    override fun onCharacteristicWrite(
        gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic, status: Int
    ) {
        Timber.v("onCharacteristicWrite: characteristic: $characteristic, status: $status")
    }

    override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
        Timber.v("onServicesDiscovered: status: $status")

        gatt.services.forEach {
            Timber.i("SERVICE ${it.uuid}")
            it.characteristics.forEach { Timber.i("CHARACTERISTIC ${it.uuid}") }
        }
    }

    override fun onPhyUpdate(gatt: BluetoothGatt, txPhy: Int, rxPhy: Int, status: Int) {
        Timber.v("onPhyUpdate: txPhy: $txPhy, rxPhy: $rxPhy, status: $status")
    }

    override fun onMtuChanged(gatt: BluetoothGatt, mtu: Int, status: Int) {
        Timber.v("onMtuChanged: mtu: $mtu, status: $status")
    }

    override fun onReliableWriteCompleted(gatt: BluetoothGatt, status: Int) {
        Timber.v("onReliableWriteCompleted: status: $status")
    }

    override fun onDescriptorWrite(gatt: BluetoothGatt, descriptor: BluetoothGattDescriptor, status: Int) {
        Timber.v("onDescriptorWrite: descriptor: $descriptor, status: $status")
    }

    override fun onCharacteristicChanged(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic) {
        Timber.v("onCharacteristicChanged: characteristic: $characteristic")
    }

    override fun onDescriptorRead(gatt: BluetoothGatt, descriptor: BluetoothGattDescriptor, status: Int) {
        Timber.v("onDescriptorRead: descriptor: $descriptor, status: $status")
    }

    override fun onPhyRead(gatt: BluetoothGatt, txPhy: Int, rxPhy: Int, status: Int) {
        Timber.v("onPhyRead: txPhy: $txPhy, rxPhy: $rxPhy, status: $status")
    }

    override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
        Timber.v("onConnectionStateChange: status: $status, newState: $newState")
    }
}
