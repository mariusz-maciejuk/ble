package com.maciejuk.ble.dbpeek.model

import com.maciejuk.ble.domain.model.DeviceInfo

data class DbPeekState(
    val records: List<DeviceInfo>,
    val loading: Boolean,
    val exception: Exception? = null
)
