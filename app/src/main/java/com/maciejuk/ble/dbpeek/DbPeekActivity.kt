package com.maciejuk.ble.dbpeek

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.maciejuk.ble.R
import com.maciejuk.ble.appnavigation.NavigationDrawerHandler
import com.maciejuk.ble.common.extensions.bind
import com.maciejuk.ble.common.extensions.injectPassingActivity
import com.maciejuk.ble.databinding.ActivityDbpeekBinding
import com.maciejuk.ble.domain.model.DeviceInfo
import com.snakydesign.livedataextensions.distinctUntilChanged
import com.snakydesign.livedataextensions.map
import kotlinx.android.synthetic.main.activity_dbpeek.dbRecords
import kotlinx.android.synthetic.main.view_appbar.toolbar
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class DbPeekActivity : AppCompatActivity() {
    private val navigationDrawerHandler: NavigationDrawerHandler by injectPassingActivity()
    private val viewModel: DbPeekViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bind<ActivityDbpeekBinding>(R.layout.activity_dbpeek) {
            viewModel = this@DbPeekActivity.viewModel
            lifecycleOwner = this@DbPeekActivity
        }
        lifecycle.addObserver(viewModel)

        setSupportActionBar(toolbar)
        lifecycle.addObserver(navigationDrawerHandler)

        viewModel.state.observe(this, Observer { Timber.d("$it") })

        setupRecordsList()
    }

    override fun onBackPressed() {
        viewModel.quit()
    }


    private fun setupRecordsList() = dbRecords.run {
        registerType<DeviceInfo>(R.layout.view_item_device_info)

        viewModel.state
            .map { it.records }
            .distinctUntilChanged()
            .observe(this@DbPeekActivity, Observer {
                data = it
            } )
    }
}
