package com.maciejuk.ble.dbpeek

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.maciejuk.ble.appnavigation.Router
import com.maciejuk.ble.dbpeek.model.DbPeekState
import com.maciejuk.ble.domain.model.DeviceInfo
import com.maciejuk.ble.repository.DeviceRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

class DbPeekViewModel(
    dispatcher: CoroutineDispatcher,
    private val router: Router,
    private val repository: DeviceRepository
) : ViewModel(), LifecycleObserver, CoroutineScope {
    private val job = Job()
    override val coroutineContext: CoroutineContext = dispatcher + job

    val state: LiveData<DbPeekState>
        get() = _state
    private val _state = MutableLiveData<DbPeekState>()


    init {
        _state.value = DbPeekState(
            records = emptyList(),
            loading = false
        )
    }


    override fun onCleared() {
        job.cancel()
        super.onCleared()
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onActivityStart() {
        if (state.value?.records.isNullOrEmpty()) loadRecords()
    }


    fun quit() {
        router.closeCurrentScreen()
    }


    private fun loadRecords() = launch {
        _state.update(loading = true)
        try {
            _state.update(records = repository.loadEveryDeviceInfo())
        } catch (exception: Exception) {
            handleException(exception)
        } finally {
            _state.update(loading = false)
        }
    }

    private fun handleException(exception: Exception) {
        Timber.tag(DbPeekViewModel::class.simpleName).e(exception)
        _state.update(exception = exception)
    }
}


private fun MutableLiveData<DbPeekState>.update(
    records: List<DeviceInfo> = value?.records.orEmpty(),
    loading: Boolean = value?.loading ?: false,
    exception: Exception? = value?.exception
) {
    @Suppress("CopyWithoutNamedArguments")
    value = value?.copy(records, loading, exception)
}
