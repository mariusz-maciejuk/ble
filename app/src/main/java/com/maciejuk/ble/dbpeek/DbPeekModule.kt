package com.maciejuk.ble.dbpeek

import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val dbPeekModule = module {
    viewModel { DbPeekViewModel(get("main"), get(), get()) }
}
