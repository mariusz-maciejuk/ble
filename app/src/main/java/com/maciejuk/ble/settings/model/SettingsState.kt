package com.maciejuk.ble.settings.model

data class SettingsState(
    val deviceName: String?,
    val loading: Boolean,
    val exception: Exception? = null
)
