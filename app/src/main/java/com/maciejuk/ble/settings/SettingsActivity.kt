package com.maciejuk.ble.settings

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.maciejuk.ble.R
import com.maciejuk.ble.appnavigation.NavigationDrawerHandler
import com.maciejuk.ble.common.extensions.bind
import com.maciejuk.ble.common.extensions.injectPassingActivity
import com.maciejuk.ble.common.extensions.observeOnce
import com.maciejuk.ble.common.extensions.setInitialValue
import com.maciejuk.ble.databinding.ActivitySettingsBinding
import com.snakydesign.livedataextensions.distinctUntilChanged
import com.snakydesign.livedataextensions.map
import kotlinx.android.synthetic.main.activity_settings.deviceNameSetting
import kotlinx.android.synthetic.main.view_appbar.toolbar
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

class SettingsActivity : AppCompatActivity() {
    private val navigationDrawerHandler: NavigationDrawerHandler by injectPassingActivity()
    private val viewModel: SettingsViewModel by viewModel()

    private val deviceNameTextWatcher = object : TextWatcher {
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            viewModel.onDeviceNameSettingChanged(s?.toString())
        }
        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bind<ActivitySettingsBinding>(R.layout.activity_settings) {
            viewModel = this@SettingsActivity.viewModel
            lifecycleOwner = this@SettingsActivity
        }
        lifecycle.addObserver(viewModel)

        setSupportActionBar(toolbar)
        lifecycle.addObserver(navigationDrawerHandler)

        viewModel.state.observe(this, Observer { Timber.d("$it") })

        setupDeviceNameTextField()
    }

    override fun onBackPressed() {
        viewModel.quit()
    }


    private fun setupDeviceNameTextField() = deviceNameSetting.run {
        addTextChangedListener(deviceNameTextWatcher)

        viewModel.state
            .map { it.deviceName }
            .distinctUntilChanged()
            .observeOnce(this@SettingsActivity, Observer {
                removeTextChangedListener(deviceNameTextWatcher)
                setInitialValue(it)
                addTextChangedListener(deviceNameTextWatcher)
            } )
    }
}
