package com.maciejuk.ble.settings

import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val settingsModule = module {
    viewModel { SettingsViewModel(get("main"), get(), get()) }
}
