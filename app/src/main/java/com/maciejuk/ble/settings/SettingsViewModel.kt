package com.maciejuk.ble.settings

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.maciejuk.ble.appnavigation.Router
import com.maciejuk.ble.repository.DeviceRepository
import com.maciejuk.ble.settings.model.SettingsState
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

class SettingsViewModel(
    dispatcher: CoroutineDispatcher,
    private val router: Router,
    private val repository: DeviceRepository
) : ViewModel(), LifecycleObserver, CoroutineScope {
    private val job = Job()
    override val coroutineContext: CoroutineContext = dispatcher + job

    val state: LiveData<SettingsState>
        get() = _state
    private val _state = MutableLiveData<SettingsState>()


    init {
        _state.value = SettingsState(
            deviceName = repository.deviceName,
            loading = false
        )
    }


    override fun onCleared() {
        job.cancel()
        super.onCleared()
    }


    fun onDeviceNameSettingChanged(value: String?) {
        repository.deviceName = value
        _state.update(deviceName = value)
    }

    fun quit() {
        router.closeCurrentScreen()
    }

    private fun handleException(exception: Exception) {
        Timber.tag(SettingsViewModel::class.simpleName).e(exception)
        _state.update(exception = exception)
    }
}


private fun MutableLiveData<SettingsState>.update(
    deviceName: String? = value?.deviceName,
    loading: Boolean = value?.loading ?: false,
    exception: Exception? = value?.exception
) {
    @Suppress("CopyWithoutNamedArguments")
    value = value?.copy(deviceName, loading, exception)
}
