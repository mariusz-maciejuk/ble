package com.maciejuk.ble.details.model

data class DetailsState(
    val requestedDeviceAddress: String,
    val connected: Boolean,
    val deviceInfo: ObtainedDeviceInfo?,
    val alreadySaved: Boolean,
    val loading: Boolean,
    val exception: Exception? = null
)

data class ObtainedDeviceInfo(
    val manufacturerName: String?,
    val deviceName: String?,
    val serialNumber: String?,
    val batteryLevel: Int?
)
