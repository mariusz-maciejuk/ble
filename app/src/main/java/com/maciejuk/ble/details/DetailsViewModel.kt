package com.maciejuk.ble.details

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.maciejuk.ble.appnavigation.Router
import com.maciejuk.ble.ble.connection.BleConnectionAdapter
import com.maciejuk.ble.ble.connection.BleDeviceInfoReader
import com.maciejuk.ble.ble.connection.model.ConnectionState
import com.maciejuk.ble.ble.connection.model.ConnectionState.CONNECTED
import com.maciejuk.ble.details.model.DetailsState
import com.maciejuk.ble.details.model.ObtainedDeviceInfo
import com.maciejuk.ble.domain.model.DeviceInfo
import com.maciejuk.ble.repository.DeviceRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.threeten.bp.LocalDateTime
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

class DetailsViewModel(
    address: String,
    dispatcher: CoroutineDispatcher,
    private val router: Router,
    private val repository: DeviceRepository,
    private val bleDeviceInfoReader: BleDeviceInfoReader
) : ViewModel(), LifecycleObserver, CoroutineScope, BleConnectionAdapter.Listener {
    private val job = Job()
    override val coroutineContext: CoroutineContext = dispatcher + job

    val state: LiveData<DetailsState>
        get() = _state
    private val _state = MutableLiveData<DetailsState>()


    init {
        _state.value = DetailsState(
            requestedDeviceAddress = address,
            connected = false,
            deviceInfo = null,
            alreadySaved = false,
            loading = false
        )

        bleDeviceInfoReader.listener = this
    }


    override fun onCleared() {
        job.cancel()
        super.onCleared()
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onActivityStart() {
        if (state.value?.deviceInfo == null) {
            state.value?.requestedDeviceAddress?.let {
                _state.update(loading = true)
                bleDeviceInfoReader.connectToDevice(it)
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onActivityStop() {
        bleDeviceInfoReader.disconnect()
    }


    override fun onConnectionStateChanged(connectionState: ConnectionState) {
        (connectionState == CONNECTED).let {
            _state.update(connected = it)
            if (it) launch {
                readProperties().join()
                checkIfSaved()
            }
        }
    }


    fun saveRequested() {
        saveInfo()
    }

    fun quit() {
        router.closeCurrentScreen()
    }


    private fun readProperties() = launch {
        _state.update(loading = true)
        try {
            bleDeviceInfoReader.run {
                _state.update(deviceInfo = ObtainedDeviceInfo(
                    manufacturerName = getManufacturerName(),
                    deviceName = getDeviceName(),
                    serialNumber = getSerialNumber(),
                    batteryLevel = getBatteryLevel()
                ))
            }
        } catch (exception: Exception) {
            handleException(exception)
        } finally {
            _state.update(loading = false)
        }
    }

    private fun checkIfSaved() = launch {
        _state.update(loading = true)
        try {
            _state.update(alreadySaved = repository.isInfoSavedForDevice(state.value?.requestedDeviceAddress.orEmpty()))
        } catch (exception: Exception) {
            handleException(exception)
        } finally {
            _state.update(loading = false)
        }
    }

    private fun saveInfo() = launch {
        _state.update(loading = true)
        try {
            createDeviceInfoRecord()?.let { repository.saveDeviceInfo(it) }
            _state.update(alreadySaved = true)
        } catch (exception: Exception) {
            handleException(exception)
        } finally {
            _state.update(loading = false)
        }
    }

    private fun createDeviceInfoRecord() = state.value?.run { DeviceInfo(
        id = null,
        address = requestedDeviceAddress,
        manufacturerName = deviceInfo?.manufacturerName,
        deviceName = deviceInfo?.deviceName,
        serialNumber = deviceInfo?.serialNumber,
        batteryLevel = deviceInfo?.batteryLevel,
        saveTime = LocalDateTime.now(),
        processedTime = null
    ) }

    private fun handleException(exception: Exception) {
        Timber.tag(DetailsViewModel::class.simpleName).e(exception)
        _state.update(exception = exception)
    }
}


private fun MutableLiveData<DetailsState>.update(
    address: String = value!!.requestedDeviceAddress,
    connected: Boolean = value?.connected ?: false,
    deviceInfo: ObtainedDeviceInfo? = value?.deviceInfo,
    alreadySaved: Boolean = value?.alreadySaved ?: false,
    loading: Boolean = value?.loading ?: false,
    exception: Exception? = value?.exception
) {
    @Suppress("CopyWithoutNamedArguments")
    value = value?.copy(address, connected, deviceInfo, alreadySaved, loading, exception)
}
