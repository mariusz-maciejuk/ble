package com.maciejuk.ble.details

import com.maciejuk.ble.common.extensions.get
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val detailsModule = module {
    viewModel { DetailsViewModel(it["device_address"], get("main"), get(), get(), get()) }
}
