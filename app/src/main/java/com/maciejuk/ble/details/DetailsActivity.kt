package com.maciejuk.ble.details

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.maciejuk.ble.BuildConfig
import com.maciejuk.ble.R
import com.maciejuk.ble.common.extensions.bind
import com.maciejuk.ble.common.extensions.composeParams
import com.maciejuk.ble.databinding.ActivityDetailsBinding
import com.maciejuk.ble.details.DetailsActivity.Companion.DEVICE_ADDRESS
import kotlinx.android.synthetic.main.view_appbar.toolbar
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

fun Intent.withDetailsParams(address: String) = putExtra(DEVICE_ADDRESS, address)

class DetailsActivity : AppCompatActivity() {
    companion object {
        const val DEVICE_ADDRESS = BuildConfig.APPLICATION_ID + ".details.params.deviceAddress"
    }

    private val viewModel: DetailsViewModel by viewModel { getViewModelParams() }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bind<ActivityDetailsBinding>(R.layout.activity_details) {
            viewModel = this@DetailsActivity.viewModel
            lifecycleOwner = this@DetailsActivity
        }
        lifecycle.addObserver(viewModel)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel.state.observe(this, Observer { Timber.d("$it") })
    }

    override fun onBackPressed() {
        viewModel.quit()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }


    private fun getViewModelParams() = composeParams(
        "device_address" to intent.getStringExtra(DEVICE_ADDRESS)
    )
}
