package com.maciejuk.ble.repository

import com.maciejuk.ble.domain.model.DeviceInfo
import com.maciejuk.ble.repository.db.DeviceDatabase
import com.maciejuk.ble.repository.db.converters.toDbDeviceInfo
import com.maciejuk.ble.repository.db.converters.toDeviceInfo
import com.maciejuk.ble.repository.prefs.DeviceSettingsProvider

class DeviceRepository internal constructor(
    private val db: DeviceDatabase,
    private val prefs: DeviceSettingsProvider
) {
    var deviceName: String?
        get() = prefs.deviceName
        set(value) { prefs.deviceName = value }


    suspend fun saveDeviceInfo(deviceInfo: DeviceInfo) {
        db.deviceInfoDao().insert(deviceInfo.toDbDeviceInfo())
    }

    suspend fun update(deviceInfoList: List<DeviceInfo>): Int {
        return db.deviceInfoDao().update(deviceInfoList.map { it.toDbDeviceInfo() })
    }

    suspend fun isInfoSavedForDevice(address: String): Boolean {
        return db.deviceInfoDao().selectFirst(address) != null
    }

    suspend fun loadUnprocessed(): List<DeviceInfo> {
        return db.deviceInfoDao().selectUnprocessed().map { it.toDeviceInfo() }
    }

    suspend fun loadProcessed(): List<DeviceInfo> {
        return db.deviceInfoDao().selectProcessed().map { it.toDeviceInfo() }
    }

    suspend fun loadEveryDeviceInfo(): List<DeviceInfo> {
        return db.deviceInfoDao().selectAll().map { it.toDeviceInfo() }
    }
}
