package com.maciejuk.ble.repository.prefs

import android.content.Context
import android.content.Context.MODE_PRIVATE
import com.maciejuk.ble.BuildConfig
import com.maciejuk.ble.common.extensions.save

internal class DeviceSettingsProvider(context: Context) {
    companion object {
        private const val SHARED_PREFS_FILE_ID = BuildConfig.APPLICATION_ID + ".settings.device"
        private const val SETTING_ID_DEVICE_NAME = BuildConfig.APPLICATION_ID + ".setting.default_account"
    }


    var deviceName: String?
        get() = prefs.getString(SETTING_ID_DEVICE_NAME, "")
        set(value) = prefs.save(SETTING_ID_DEVICE_NAME, value.orEmpty())


    private val prefs = context.applicationContext.getSharedPreferences(SHARED_PREFS_FILE_ID, MODE_PRIVATE)
}
