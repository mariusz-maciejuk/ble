package com.maciejuk.ble.repository.db.converters

import androidx.room.TypeConverter
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

class DateTimeConverters {
    private val formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME

    @TypeConverter
    fun toString(localDateTime: LocalDateTime?): String? = localDateTime?.format(formatter)

    @TypeConverter
    fun toLocalDateTime(string: String?): LocalDateTime? = string?.let { formatter.parse(it, LocalDateTime::from) }
}
