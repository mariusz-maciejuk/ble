package com.maciejuk.ble.repository.db.converters

import com.maciejuk.ble.common.extensions.toCurrentZone
import com.maciejuk.ble.common.extensions.toUtcZone
import com.maciejuk.ble.domain.model.DeviceInfo
import com.maciejuk.ble.repository.db.model.DbDeviceInfo

internal fun DeviceInfo.toDbDeviceInfo() = DbDeviceInfo(
    id = id,
    address = address,
    manufacturerName = manufacturerName,
    deviceName = deviceName,
    serialNumber = serialNumber,
    batteryLevel = batteryLevel,
    saveTime = saveTime?.toUtcZone(),
    processedTime = processedTime?.toUtcZone()
)

internal fun DbDeviceInfo.toDeviceInfo() = DeviceInfo(
    id = id,
    address = address.orEmpty(),
    manufacturerName = manufacturerName,
    deviceName = deviceName,
    serialNumber = serialNumber,
    batteryLevel = batteryLevel,
    saveTime = saveTime?.toCurrentZone(),
    processedTime = processedTime?.toCurrentZone()
)
