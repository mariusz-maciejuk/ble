package com.maciejuk.ble.repository.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.maciejuk.ble.repository.db.model.DbDeviceInfo

@Dao
internal interface DeviceInfoDao {
    @Insert
    suspend fun insert(record: DbDeviceInfo)

    @Update
    suspend fun update(deviceInfoList: List<DbDeviceInfo>): Int

    @Query("SELECT * FROM device_info WHERE address == :address LIMIT 1")
    suspend fun selectFirst(address: String): DbDeviceInfo?

    @Query("SELECT * FROM device_info WHERE processedTime IS NULL")
    suspend fun selectUnprocessed(): List<DbDeviceInfo>

    @Query("SELECT * FROM device_info WHERE processedTime IS NOT NULL")
    suspend fun selectProcessed(): List<DbDeviceInfo>

    @Query("SELECT * FROM device_info ORDER BY saveTime DESC")
    suspend fun selectAll(): List<DbDeviceInfo>
}
