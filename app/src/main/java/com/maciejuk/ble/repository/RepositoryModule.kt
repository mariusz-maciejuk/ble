package com.maciejuk.ble.repository

import com.maciejuk.ble.repository.db.getDeviceDatabase
import com.maciejuk.ble.repository.prefs.DeviceSettingsProvider
import org.koin.dsl.module.module

val repositoryModule = module {
    single { getDeviceDatabase(get()) }
    single { DeviceSettingsProvider(get()) }
    single { DeviceRepository(get(), get()) }
}
