package com.maciejuk.ble.repository.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.threeten.bp.LocalDateTime

@Entity(tableName = "device_info")
internal data class DbDeviceInfo(
    @PrimaryKey(autoGenerate = true)
    val id: Long? = null,
    val address: String? = null,
    val manufacturerName: String? = null,
    val deviceName: String? = null,
    val serialNumber: String? = null,
    val batteryLevel: Int? = null,
    val saveTime: LocalDateTime? = null,
    val processedTime: LocalDateTime? = null
)
