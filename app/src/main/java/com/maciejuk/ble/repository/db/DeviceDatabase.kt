package com.maciejuk.ble.repository.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.maciejuk.ble.repository.db.converters.DateTimeConverters
import com.maciejuk.ble.repository.db.model.DbDeviceInfo

@Database(entities = [DbDeviceInfo::class], version = 1, exportSchema = false)
@TypeConverters(DateTimeConverters::class)
internal abstract class DeviceDatabase : RoomDatabase() {
    abstract fun deviceInfoDao(): DeviceInfoDao
}

internal fun getDeviceDatabase(context: Context) = Room
    .databaseBuilder(context, DeviceDatabase::class.java, "devices.db")
    .fallbackToDestructiveMigration()
    .build()
