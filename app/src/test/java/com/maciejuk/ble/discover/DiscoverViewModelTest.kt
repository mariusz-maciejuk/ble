package com.maciejuk.ble.discover

import com.jraska.livedata.test
import com.maciejuk.ble.appnavigation.Router
import com.maciejuk.ble.ble.scanner.BleScanner
import com.maciejuk.ble.provideDevice
import io.kotlintest.IsolationMode
import io.kotlintest.specs.FreeSpec
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class DiscoverViewModelTest : FreeSpec({
    val router = mockk<Router>(relaxed = true)
    val bleScanner = mockk<BleScanner>(relaxed = true)

    "On enter discover screen" - {
        val viewModel = DiscoverViewModel(
            dispatcher = Dispatchers.Unconfined,
            router = router,
            bleScanner = bleScanner
        )

        val viewModelStateObserver = viewModel.state.test()


        "initial state has been created" - { viewModelStateObserver.assertHasValue() }

        "don't load anything" - { viewModelStateObserver.assertValue { !it.loading } }

        "don't initiate auto discovery" - { viewModelStateObserver.assertValue { !it.discovering } }

        "With disabled BT" - {
            every { bleScanner.btEnabled } answers { false }

            "On start click" - {
                viewModel.onStartStopClicked()

                "request to enable BT" - { verify { router.showBtSettings(DiscoverViewModel.BT_ENABLE_REQUEST) } }

                "On BT enabled" - {
                    viewModel.onBtEnabled()

                    "resume starting discovery" - { verify { viewModel.onStartStopClicked() } }
                }
            }
        }

        "With enabled BT" - {
            every { bleScanner.btEnabled } answers { true }

            "On start click" - {
                viewModel.onStartStopClicked()

                "change discovery status" - { viewModelStateObserver.assertValue { it.loading && it.discovering } }

                "start discovery" - { verify { bleScanner.startScan() } }

                "On stop click" - {
                    viewModel.onStartStopClicked()

                    "change discovery status" - {
                        viewModelStateObserver.assertValue { !it.loading && !it.discovering }
                    }

                    "start discovery" - { verify { bleScanner.stopScan() } }
                }
            }
        }

        "On new device discovered" - {
            val newDevice = provideDevice()
            viewModel.onDeviceDiscovered(newDevice)

            "include it on the list" - {
                viewModelStateObserver.assertValue { it.discoveredDevices.contains(newDevice) }
            }

            "On device details requested" - {
                viewModel.onDeviceDetailsRequested(newDevice)

                "show details screen" - { verify { router.showDetails(newDevice.address) } }
            }
        }

        "On quit" - {
            viewModel.quit()

            "close the screen" - { verify { router.closeCurrentScreen() } }
        }
    }
}) {
    override fun isolationMode() = IsolationMode.InstancePerLeaf
}
