@file:Suppress("PackageDirectoryMismatch")
package io.kotlintest.provided

import androidx.arch.core.executor.ArchTaskExecutor
import androidx.arch.core.executor.TaskExecutor
import io.kotlintest.AbstractProjectConfig

@Suppress("unused")
object ProjectConfig : AbstractProjectConfig() {
    override fun beforeAll() {
        ArchTaskExecutor.getInstance().setDelegate(object : TaskExecutor() {
            override fun executeOnDiskIO(runnable: Runnable) = runnable.run()
            override fun postToMainThread(runnable: Runnable) = runnable.run()
            override fun isMainThread() = true
        })
    }

    override fun afterAll() {
        ArchTaskExecutor.getInstance().setDelegate(null)
    }
}
