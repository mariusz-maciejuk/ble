package com.maciejuk.ble

import com.maciejuk.ble.domain.model.Device

fun provideDevice(
    name: String = "Test Device Name",
    address: String = "00:0A:E6:3E:FD:E1"
) = Device(name, address)
