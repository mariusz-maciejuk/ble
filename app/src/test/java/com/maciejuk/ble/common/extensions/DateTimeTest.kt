package com.maciejuk.ble.common.extensions

import org.amshove.kluent.shouldEqual
import org.junit.jupiter.api.Test
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId

class DateTimeTest {
    @Test
    fun `convert local LocalDateTime to UTC based LocalDateTime`() {
        val local = LocalDateTime.of(2019, 10,24, 20, 20, 0)

        val utc = local.toUtcZone(currentZone = ZoneId.of("UTC+1"))

        utc shouldEqual local.minusHours(1)
    }

    @Test
    fun `convert UTC based LocalDateTime to local LocalDateTime`() {
        val utc = LocalDateTime.of(2019, 10,24, 20, 20, 0)

        val local = utc.toCurrentZone(currentZone = ZoneId.of("UTC+1"))

        local shouldEqual utc.plusHours(1)
    }
}
