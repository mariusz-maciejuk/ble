### UI

The assignment doesn't specify pretty much any UI except for the editable text field on settings screen.
I took the liberty of adding a few bits from myself to make the app a little bit more interactive so even non technical person could play with it a little without having to use dev tools.

### Settings

Because of the [UI additions](###UI) the requested setting lost its purpose, so it doesn't have any impact on the app. It's still functional though.
It's easy to downgrade the app to get the exact behavior defined in the requirements without modifying the `ble` package. 
I know there's another API for settings screens. Didn't use it because don't see the need.

### Edge cases

A number of edge cases I'm aware of is left unhandled. Especially the ones related to device's state, granted permissions, or connectivity. Trying to cover all of them would take too much time for a simple recruitment task.

### Repeated code

Depending on shape of an actual project it might be beneficial to introduce base classes for `ViewModel`, `State` and `Activity` classes. It'd allow to cut down some repetitions (especially around binding those with each other) and shape the architecture more vividly.

### Architecture layers

Depending on actual project it might be worth to add additional layers that will take on responsibility of purely UI logic, business logic, UI actions and so on. It'd also enable the developer to work in better separation from UI, and test more without being forced to use instrumentation tests.
Approach that's been applied here requires instrumentation to run UI tests, which could be avoided in exchange for a higher maintenance cost.

### BT connectivity

Entire `ble` package is proof of concept rather than a production ready code.
It needs a couple of additional abstraction layers depending on an actual project. The layers needs to handle things like threading and asynchronous callbacks, safe execution of long running commands and lifecycle independent from client's, retrieving and parsing actual values, separately in order to stay maintainable. On the very top of that it might be nice to have several small, specialised facades.

### Read BT characteristics

I don't have any device on which I could test retrieving the specified in assignment characteristics. So I added a couple of my own, and implemented the required ones according to specification, but I didn't have chance to verify if the implementation is 100% accurate.

### DB

I chose another format to store date time. I still don't store information about the origin time zone, nor the time zone irregularities like DST though. Would have went different way if the requirements were more demanding.
I also intentionally omitted `has_been_processed` flag as it's redundant.
You can see what's actually stored in the DB without using ADB, using `DB Peek` screen.

### Background processing

Final solution can be implemented in many different ways and may use completely different APIs depending on nature of an actual job. Here I took the simplest possible path. Didn't even bother wrapping it any testable abstraction.

### Tests

There's no time to write tests that would cover everything, but I prepared two test classes - one for logic container and the other for utility class. They illustrate the concept well enough.
Completely skipped any instrumentation tests.

### Running tests

Do not use IDE, nor the IDE's embedded terminal. Use your regular, day-to-day terminal and simply fire up a Gradle task you're interested in. `tDUT` for instance.

### Compiling

The project should be ready for compiling out of the box. Just make sure you have the required dependencies installed and at right versions (check out the project level `build.gradle`).

### Signing configuration

I'm aware about using the same certificate for debug and release is bad practice. Did it intentionally to make your life easier while compiling.
